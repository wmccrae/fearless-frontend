window.addEventListener('DOMContentLoaded', async () => {
    // URL for the list of states
    const statesUrl = 'http://localhost:8000/api/states/';

    try {
        // Fetch the URL for the list of states
        const categorySelect = document.getElementById("state");
        categorySelect.disabled = true;
        const response = await fetch(statesUrl);

        if(!response.ok) {
            // Print the response, if it wasn't ok.
            console.log("Invalid states list response: ", response);
        } else {
            // If the response was okay, get the JSON data from our request.
            const statesList = await response.json();
            for (const state of statesList.states) {
                const option = document.createElement('option');
                option.innerHTML = state.name;
                option.value = state.abbreviation;
                categorySelect.appendChild(option);
            }
        }
        categorySelect.disabled = false;


    // upon 'submit',get the form data the user entered
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

        // send the data to the server
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                    'Content-Type': 'application/json',
                },
            };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            }
    });



    } catch (e) {
        // Print the details of the error that was raised.
        console.log("Error raised on URL fetch: ", e);
    }



})
