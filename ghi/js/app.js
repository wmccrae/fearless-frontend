// This function creates a card for a conference displayed on the website.
// Using Bootstrap the following codes are added:
// w-100: width 100% of parent
// shadow: standard shadow for each card
// mb-3: vertical space between cards
function createCard(name, description, pictureUrl, location, startDate, endDate) {
    return `
      <div class="card w-100 shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="text-muted">${location}</p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <p class="card-text">${startDate} - ${endDate}</p>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    // URL for the list of conferences
    const url = 'http://localhost:8000/api/conferences/';

    try {
        // Request the list of conferences and see what the server response is.
        const response = await fetch(url);

        if (!response.ok) {
          // Print the response, if it wasn't OK.
          console.log("Invalid Conference List response: ", response);
        } else {
            // If the server request was okay, get the JSON data from our request.
          const data = await response.json();

          // Create a card for every conference in the list.
          // Create in index so we can use columns.
          let index = 0;
          for (let conference of data.conferences) {
            if (index < 3) {
              index += 1;
            } else {
              index = 1;
            }
            // Request the conference details and see what the server response is.
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);

            if (detailResponse.ok) {
                // If the server request was okay, get the JSON data from our request.
                const details = await detailResponse.json();

                // Parse the conference title.
                const confTitle = details.conference.name;

                // Parse the conference description.
                const confDetails = details.conference.description;

                // Parse the conference's picture URL.
                const pictureUrl = details.conference.location.picture_url;

                // Parse the conference location.
                const confLocation = details.conference.location.name;

                // Parse the conference's start and end dates.
                const confStart = details.conference.starts; // fetch data
                const start = new Date(confStart); // parse out into datetime
                let startString = start.toLocaleDateString(); // turn into datestring

                const confEnd = details.conference.ends; // fetch data
                const end = new Date(confEnd); // parse out into datetime
                let endString = end.toLocaleDateString(); // turn into datestring

                // Call the custom function above to write the HTML to create a conference card for the website.
                const html = createCard(
                  confTitle,
                  confDetails,
                  pictureUrl,
                  confLocation,
                  startString,
                  endString);
                // Reference each column's unique id to put the card in the
                // proper column.
                const column = document.querySelector(`#col${index}`);
                column.innerHTML += html;
            } else {
              console.log("Invalid Conference Details response: ", detailResponse);
            }
          }
        }
      } catch (e) {
        // Print the details of the error that was raised.
        console.log("Error raised: ", e);
      }

  });
