window.addEventListener('DOMContentLoaded', async () => {
    // URL for the list of locations
    const locsUrl = 'http://localhost:8000/api/locations/';

    try {
        // Fetch the URL for the list of locations
        const categorySelect = document.getElementById("location");
        categorySelect.disabled = true;
        const response = await fetch(locsUrl);

        if(!response.ok) {
            // Print the response, if it wasn't ok.
            console.log("Invalid location list response: ", response);
        } else {
            // If the response was okay, get the JSON data from our request.
            const locsList = await response.json();

            // Populate the location drop-down menu.
            for (const location of locsList.locations) {
                const option = document.createElement('option');
                // the value shown to the user
                option.innerHTML = location.name;
                // the value sent out to the database
                option.value = location.id;
                // option.id = location.id; Adding an option id is not necessary for this api.
                categorySelect.appendChild(option);
            }
        }
        categorySelect.disabled = false;


        // upon 'submit',get the form data the user entered
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            // send the data to the server
            const conferencesUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                        'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferencesUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
            } else {
                console.log("Invalid JSON content.");
            }
        });



    } catch (e) {
        // Print the details of the error that was raised.
        console.log("Error raised on URL fetch: ", e);
    }



})
