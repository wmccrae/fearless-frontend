// This function returns a page with a form for entering a new presentation into
// the database.

import React, {useEffect, useState} from 'react';

function PresentationForm () {
    // Define a function to handle submitting form data.
    const handleSubmit = async (event) => {
        event.preventDefault();

        // Create an empty JSON object to return the form data
        // to the database.
        const data = {};

        // Assign the form data to the JSON object.
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        // Send the data to the server to save the new
        // presentation
        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            // turn the data into a JSON string
            body: JSON.stringify(data),
            headers: {
                    'Content-Type': 'application/json',
                },
            };
        const response = await fetch(presentationUrl, fetchConfig);

        if (response.ok) {
            const newPresentation = await response.json();
            // Reset the form.
            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    // Via useState, create a variable to hold our list
    // of conferences with an initial value of an empty
    // string.
    const [conferences, setConferences] = useState([]);

    // Use the useState hook to store the form's values in
    // the component's state, with a default initial value
    // of an empty string.
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    // Create event handlers to store the user's inputs.
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    // Fetch the list of conferences.
    const fetchData = async () => {
        // URL for the list of Conferences
        const conferencesUrl = 'http://localhost:8000/api/conferences/';

        // Get the server response for the list of conferences.
        const response = await fetch(conferencesUrl);

        if (response.ok) {
            // If the response is okay, get the list of conferences.
            const conferencesData = await response.json()

            // Display the list of conferences.
            setConferences(conferencesData.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // Return the HTML to be rendered on the page.
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" value={presenterName} className="form-control"/>
                            <label htmlFor="presenter_name">Presenter Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePresenterEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" value={presenterEmail} className="form-control"/>
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" value={companyName} className="form-control"/>
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" value={title} className="form-control"/>
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea onChange={handleSynopsisChange} id="synopsis" rows="3" name="synopsis" value={synopsis} className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleConferenceChange} required name="conference" id="conference" className="form-select" value={conference}>
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => {
                                    return (
                                        <option key={conference.id} value={conference.id}>
                                            {conference.name}
                                        </option>
                                    )
                                })
                                }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default PresentationForm;
