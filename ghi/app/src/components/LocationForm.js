// This function returns a page with a form for entering a new location into
// the database.

import React, {useEffect, useState} from 'react';

function LocationForm () {
    const handleSubmit = async (event) => {
        event.preventDefault();

        // Create an empty JSON object
        const data = {};

        data.name = name;
        data.room_count = roomCount;
        data.city = city;
        data.state = state;

        // send the data to the server
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            // turn the data into a JSON string
            body: JSON.stringify(data),
            headers: {
                    'Content-Type': 'application/json',
                },
            };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();

            setName('');
            setRoomCount('');
            setCity('');
            setState('');
            }
    }

    // Via useState, create a variable 'states' and a method 'setStates'
    // with the initial value set to an empty string.
    const [states, setStates] = useState([]);

    // Set the useState hook to store the form's values in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }
    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }
    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const fetchData = async () => {
        // URL for the list of states
        const statesUrl = 'http://localhost:8000/api/states/';

        // Get the server response for the list of states
        const response = await fetch(statesUrl);

        if (response.ok) {
            // If the response is okay, get the list of states.
            const statesData = await response.json();
            setStates(statesData.states);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // Return the HTML to be rendered on the page.
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Name" required type="text" name="name"
                       value={name} id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" value={roomCount} id="room_count" className="form-control"/>
                        <label htmlFor="room_count">Room Count</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleCityChange} placeholder="City" required type="text" name="city" value={city} id="city" className="form-control"/>
                        <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleStateChange} required id="state" name="state" value={state} className="form-select">
                        <option value="">Choose a State</option>
                        {states.map(state => {
                            return (
                                <option key={state.abbreviation} value={state.abbreviation}>
                                    {state.name}
                                </option>
                            )
                        })

                        }
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
      </div>
    );
}

export default LocationForm;
