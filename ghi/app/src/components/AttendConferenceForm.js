// This function returns a page with a form for a new attendee.

import React, {useEffect, useState} from 'react';

function AttendConferenceForm () {
    const handleSubmit = async (event) => {
        event.preventDefault();

        // Create an empty JSON object to return the form data
        // to the database.
        const data = {};

        // Assign the form data to the JSON object.
        data.name = name;
        data.email = email;
        data.company_name = "";
        data.conference = conference;

        // send the data to the server to save the new attendee
        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            // turn the data into a JSON string
            body: JSON.stringify(data),
            headers: {
                    'Content-Type': 'application/json',
                },
            };
        const response = await fetch(attendeesUrl, fetchConfig);

        // If the response is okay, send the new attendee data to the server.
        if (response.ok) {
            const newAttendee = await response.json();
            // Reset the form
            setName('');
            setEmail('');
            setConference('');
            }
    }

    // Via useState, create a variable to hold our list
    // of conferences with an initial value of an empty
    // string.
    const [conferences, setConferences] = useState([]);

    // Use the useState hook to store the form's values in
    // the component's state, with a default initial value
    // of an empty string.
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [conference, setConference] = useState('');

    // Create event handlers to store the user's inputs.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }


    const fetchData = async () => {
        // URL for the list of Conferences
        const conferencesUrl = 'http://localhost:8000/api/conferences/';

        // Get the server response for the list of conferences.
        const response = await fetch(conferencesUrl);

        if (response.ok) {
            // If the response is okay, get the list of conferences.
            const conferencesData = await response.json()

            // Display the list of conferences.
            setConferences(conferencesData.conferences);

            // Now that we have the list of conferences, add the 'd-none' class
            // to the loading icon to hide it...
            const spinnerClasses = document.getElementById("loading-conference-spinner");
            spinnerClasses.classList.add("d-none");

            // ...and remove the 'd-none' class from the select tag so the list of
            // conferences is visible.
            const selectClasses = document.getElementById("conference");
            selectClasses.classList.remove("d-none");
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // Return the HTML to be rendered on the page.
    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="./logo.svg"/>
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                        <form onSubmit={handleSubmit} id="create-attendee-form">
                            <h1 className="card-title">It's Conference Time!</h1>
                            <p className="mb-3">
                                Please choose which conference
                                you'd like to attend.
                            </p>
                            <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                <div className="spinner-grow text-secondary" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleConferenceChange} name="conference" value={conference} id="conference" className="form-select d-none" required>
                                    <option value="">Choose a conference</option>
                                    {conferences.map(conference => {
                                        return (
                                            <option key={conference.href} value={conference.href}>
                                                {conference.name}
                                            </option>
                                        )
                                    })
                                    }
                                </select>
                            </div>
                            <p className="mb-3">
                            Now, tell us about yourself.
                            </p>
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" value={name} name="name" className="form-control"/>
                                        <label htmlFor="name">Your full name</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input onChange={handleEmailChange} required placeholder="Your email address" type="email" value={email} id="email" name="email" className="form-control"/>
                                        <label htmlFor="email">Your email address</label>
                                    </div>
                                </div>
                            </div>
                            <button className="btn btn-lg btn-primary">I'm going!</button>
                        </form>
                        <div className="alert alert-success d-none mb-0" id="success-message">
                            Congratulations! You're all signed up!
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AttendConferenceForm;
