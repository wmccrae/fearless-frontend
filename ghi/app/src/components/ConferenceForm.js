// This function returns a page with a form for entering a new location into
// the database.

import React, {useEffect, useState} from 'react';

function ConferenceForm () {
    const handleSubmit = async (event) => {
        event.preventDefault();

        // Create an empty JSON object
        const data = {};

        // Assign the form data to the JSON object
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        // send the data to the server to save the new conference
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            // turn the data into a JSON string
            body: JSON.stringify(data),
            headers: {
                    'Content-Type': 'application/json',
                },
            };
        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            const newConference = await response.json();
            // Reset the form.
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
            }
    }

    // Via useState, create a variable 'locations' and a method 'setLocations'
    // with the initial value set to an empty string. This is going to hold
    // our list of locations.
    const [locations, setLocations] = useState([]);

    // Set the useState hook to store the form's values in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        // URL for the list of Locations
        const locationsUrl = 'http://localhost:8000/api/locations/';

        // Get the server response for the list of locations.
        const response = await fetch(locationsUrl);

        if (response.ok) {
            // If the response is okay, get the list of locations.
            const locationsData = await response.json();
            setLocations(locationsData.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // Return the HTML to be rendered on the page.
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" value={name} id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="starts">Start Date</label>
                            <input onChange={handleStartsChange} type="date" id="starts" name="starts" value={starts} className="form-control"/>
                            </div>
                        <div className="mb-3">
                            <label htmlFor="ends">End Date</label>
                            <input onChange={handleEndsChange} type="date" id="ends" name="ends" value={ends} className="form-control"/>
                            </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <textarea onChange={handleDescriptionChange} className="form-control" name="description" value={description} id="description" rows="4"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentationsChange} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" value={maxPresentations} className="form-control"/>
                            <label htmlFor="max_presentations">Maximum Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required type="number" name="max_attendees" value={maxAttendees}  id="max_attendees" className="form-control"/>
                            <label htmlFor="max_presentations">Maximum Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required id="location" name="location" value={location} className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                )
                            })
                            }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;
