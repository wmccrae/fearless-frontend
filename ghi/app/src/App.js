import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './components/AttendeesList';
import LocationForm from './components/LocationForm';
import ConferenceForm from './components/ConferenceForm';
import AttendConferenceForm from './components/AttendConferenceForm';
import PresentationForm from "./components/PresentationForm";
import MainPage from "./components/MainPage";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    // This is a JSX fragment used to wrap these two HTML elements in one tag.
    // React only wants one HTML element returned (with its children). <Nav />
    // and <div className="container"> are two different HTML elements side-by-side,
    // and React doesn't know which to return. Wrapping them in a JSX fragment or
    // even a simple <div></div> turns them into one HTML element so they can be
    // returned by this function.
    <>
      <BrowserRouter>
        <Nav />
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="attendees">
              <Route path="" element={<AttendeesList attendees={props.attendees} />} />
              <Route path="new" element={<AttendConferenceForm />} />
            </Route>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="presentations">
              <Route path="new" element={<PresentationForm />} />
            </Route>
          </Routes>
      </BrowserRouter>


    </>
  );
}

export default App;
